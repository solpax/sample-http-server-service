﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;

namespace TestService
{
    struct chunk
    {
        public string header;
        public byte[] data;
    }

    /// <summary>
    /// A simple asynchronous HTTP server
    /// </summary>
    public class TestClass1 : IDisposable
    {
        private HttpListener _Listener = null;
        private int portNo = 8080;

        private object _LockObj = new object();
        private int _RequestCount = 0;

        public TestClass1()
        {
            _Listener = new HttpListener();
            _Listener.Prefixes.Add("http://*:"+portNo.ToString()+"/");
            _Listener.Start();
            Listen();
        }

        private void Listen()
        {
            if (_Listener != null && _Listener.IsListening)
            {
                _Listener.BeginGetContext(new AsyncCallback(ProcessResponse), this);
            }
        }

        private void Stop()
        {
            if (_Listener != null)
            {
                _Listener.Stop();
                //loop while waiting for pending requests before calling Close, per MSDN:
                //"This method shut downs the HttpListener object without processing queued requests. Any pending requests are unable to complete."
                int loopcount = 0;
                while (_RequestCount > 0 && loopcount < 10)
                {
                    System.Threading.Thread.Sleep(50);
                    loopcount++;
                }
                //disposes of listener too
                _Listener.Close();
                _Listener = null;
            }
        }

        public void Dispose(){
            Stop();
        }

        private static String GetBoundary(String ctype)
        {
            if (!String.IsNullOrEmpty(ctype))
            {
                string[] parts1 = ctype.Split(';');
                if (parts1.Length > 1)
                {
                    string[] parts2 = parts1[1].Split('=');
                    if (parts2.Length > 1)
                    {
                        return "--" + parts2[1];
                    }
                }
            }
            return "";
        }

        private static void ProcessResponse( IAsyncResult result ) {

            TestClass1 server = result.AsyncState as TestClass1;
            //listen for next request before we process this one
            server.Listen();
            int reqCount = 0;
            int maxPostSize = 1024 * 1024 * 20; // 20 megabytes
            lock (server._LockObj)
            {
                reqCount = server._RequestCount;
                server._RequestCount++;
            }
            try
            {
                //demonstrate multi-threaded response handling
                //int randomWaitSeconds = (new System.Random()).Next(0, 10);
                //System.Threading.Thread.Sleep(randomWaitSeconds * 1000);
                HttpListener httpListener = server._Listener;
                if (httpListener == null) return;
                HttpListenerContext context = httpListener.EndGetContext(result);
                HttpListenerRequest request = context.Request;
                string qryStr = request.QueryString.ToString();
                string boundaryStr = GetBoundary(request.ContentType);
                string outStr = "";
                string headers = "";
                if (request.HasEntityBody)
                {
                    byte[] inBytes;
                    using (BinaryReader reader = new BinaryReader(request.InputStream))
                    {
                        inBytes = reader.ReadBytes(maxPostSize);
                    }
                    List<chunk> chunks; ;//holds posted data and files
                    if (String.IsNullOrEmpty(boundaryStr))
                    {
                        chunk singleChunk = new chunk();
                        singleChunk.header = request.ContentType;
                        singleChunk.data = inBytes;
                        chunks = new List<chunk>();
                        chunks.Add(singleChunk);
                    }
                    else
                    {
                        chunks = getChunks(boundaryStr, inBytes);
                    }
                    outStr = Encoding.UTF8.GetString(inBytes);
                }
                context.Response.ContentType = "text/html";
                TextWriter writer = new StreamWriter(context.Response.OutputStream, System.Text.Encoding.UTF8);
                writer.WriteLine("<p>boundary is </p><p>\r\n" + boundaryStr + "\r\n</p>\r\n");
                writer.WriteLine("<p>query is </p><p>\r\n" + qryStr + "\r\n</p>\r\n");
                writer.WriteLine("<p>headers </p><pre>\r\n" + headers + "\r\n</pre>\r\n");
                writer.WriteLine("<p>received</p><pre>"+outStr+"</pre>\r\n");
                //writer.WriteLine("<html><body><b>Page asked at {0}</b>URL:{1}<br/>Other requests: {2}</body></html>", System.DateTime.Now, request.Url,reqCount.ToString());
                writer.Flush();
                writer.Close();
                context.Response.Close();
            }
            catch (Exception x) { }
            finally
            {
                lock (server._Listener)
                {
                    server._RequestCount--;
                }
            }
        }

        private static List<chunk> getChunks(string boundaryStr, byte[] inBytes)
        {   
            List<chunk> chunks = new List<chunk>();
            if (String.IsNullOrEmpty(boundaryStr))
            {
                return null;
            }
            List<int> positions = IndexOfSequence(inBytes, Encoding.ASCII.GetBytes(boundaryStr), 0);
            if (positions.Count > 1)
            {
                byte newline = (byte)'\n';
                for (int i = 0; i < positions.Count - 1; i++)
                {
                    //go to end of boundary line
                    int headstart = Array.IndexOf<byte>(inBytes, newline, positions[i]);
                    //go to end of next line
                    int headend = Array.IndexOf<byte>(inBytes, newline, headstart + 1);
                    int len = 3;
                    int end2 = 0;
                    while (headend > 0 && len > 2)
                    {
                        //see if the next line is blank
                        end2 = Array.IndexOf<byte>(inBytes, newline, headend + 1);
                        len = end2 - headend;
                        headend = end2;
                    }
                    if (headend == -1) headend = inBytes.Length;
                    byte[] headBytes = new byte[headend - headstart];
                    Buffer.BlockCopy(inBytes, headstart, headBytes, 0, (headend - headstart));
                    string header = Encoding.ASCII.GetString(headBytes);
                    if (i < positions.Count - 1)
                    {
                        int contentStart = headend;
                        int contentEnd = positions[i + 1];
                        if (inBytes[contentStart] == (byte)10) contentStart++;//trim whitespace
                        while (inBytes[contentEnd - 1] == (byte)10 || inBytes[contentEnd - 1] == (byte)13) contentEnd--;//trim whitespace
                        int contentLen = contentEnd - contentStart;
                        if (contentLen > 0)
                        {
                            byte[] contentBytes = new byte[contentLen];
                            Buffer.BlockCopy(inBytes, contentStart, contentBytes, 0, contentLen);
                            chunk newChunk = new chunk();
                            newChunk.header = header.Trim();
                            newChunk.data = contentBytes;
                            chunks.Add(newChunk);
                        }
                    }
                }
            }
            return chunks;
        }

        public static List<int> IndexOfSequence(byte[] buffer, byte[] pattern, int startIndex)
        {
            List<int> positions = new List<int>();
            int i = Array.IndexOf<byte>(buffer, pattern[0], startIndex);
            while (i >= 0 && i <= buffer.Length - pattern.Length)
            {
                byte[] segment = new byte[pattern.Length];
                Buffer.BlockCopy(buffer, i, segment, 0, pattern.Length);
                if (ByteArrayCompare(segment,pattern))
                    positions.Add(i);
                i = Array.IndexOf<byte>(buffer, pattern[0], i + pattern.Length);
            }
            return positions;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);

        static bool ByteArrayCompare(byte[] b1, byte[] b2)
        {
            // Validate buffers are the same length.
            // This also ensures that the count does not exceed the length of either buffer.  
            return b1.Length == b2.Length && memcmp(b1, b2, b1.Length) == 0;
        }
    }
}