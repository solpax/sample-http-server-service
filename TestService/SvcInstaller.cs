﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using System.Text;

namespace TestService
{
    [RunInstaller(true)]
    public class SvcInstaller: Installer
    {
        public SvcInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.DisplayName = Program.SVCNAME;
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = Program.SVCNAME;
            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }

        //this works
        //protected override void OnBeforeInstall(System.Collections.IDictionary savedState)
        //{
        //    Context.Parameters["assemblypath"] += "\" /test";
        //    base.OnBeforeInstall(savedState);
        //}

        //protected override void OnBeforeUninstall(System.Collections.IDictionary savedState)
        //{
        //    //Context.Parameters["assemblypath"] += "\" /service";
        //    base.OnBeforeUninstall(savedState);
        //}

        public static void Install(string args)
        {
            TransactedInstaller ti = new TransactedInstaller();
            SvcInstaller mi = new SvcInstaller();
            ti.Installers.Add(mi);
            String path = String.Format("/assemblypath={0}",
            System.Reflection.Assembly.GetExecutingAssembly().Location);
            String[] cmdline = { path };
            InstallContext ctx = new InstallContext("", cmdline);
            ctx.Parameters["assemblypath"] += "\" " + args;
            ti.Context = ctx;
            ti.Install(new System.Collections.Hashtable());
        }

        public static void Uninstall(string args)
        {
            TransactedInstaller ti = new TransactedInstaller();
            SvcInstaller mi = new SvcInstaller();
            ti.Installers.Add(mi);
            String path = String.Format("/assemblypath={0}",
            System.Reflection.Assembly.GetExecutingAssembly().Location);
            String[] cmdline = { path };
            InstallContext ctx = new InstallContext("", cmdline);
            ctx.Parameters["assemblypath"] += "\" " + args;
            ti.Context = ctx;
            ti.Uninstall(null);
        }
    }
}
