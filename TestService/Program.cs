﻿using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using System.IO;
using System.Security.Principal;
using System.Diagnostics;

namespace TestService
{
    class Program : ServiceBase
    {
        public const string SVCNAME = "TestService";

        private static Program _ServiceInstance = null;

        private static readonly Type _ServiceClass = typeof(TestClass1);
        private static object _ServiceClassObject = null;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;

            if (System.Environment.UserInteractive)
            {
                if (args != null && args.Length > 0)
                {
                    string parameter = string.Concat(args);
                    switch (parameter)
                    {
                        case "--install":
                            if (!IsUserAdministrator())
                            {
                                Console.WriteLine("\r\n ** Must be run as Adminstrator to install ** ");
                                return;
                            }
                            Console.WriteLine("Starting installation...");
                            SvcInstaller.Install("test1 test2 test3");
                            //ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
                            break;
                        case "--uninstall":
                            if (!IsUserAdministrator())
                            {
                                Console.WriteLine("\r\n ** Must be run as Adminstrator to un-install ** ");
                                return;
                            }
                            SvcInstaller.Uninstall("test1 test2 test3");
                            //ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
                            break;
                        case "--console":
                            Console.WriteLine("Starting service in console mode...");
                            Program.StartService();
                            Console.WriteLine("Press any key to quit");
                            Console.ReadKey();
                            Program.StopService();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Usage:");
                    Console.WriteLine("--install | --uninstall | --console");
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey();
                    return;
                }
            }
            else
            {
                File.WriteAllText("C:\\temp\\TestService.log", string.Join("\t",args));
                ServiceBase.Run(new Program());
            }
        }

        public static bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            try
            {
                //get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            return isAdmin;
        }

        private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            WriteToLog(((Exception)e.ExceptionObject).Message + ((Exception)e.ExceptionObject).InnerException.Message,EventLogEntryType.Error);
            if (Program._ServiceClassObject != null)
            {
                Program.StopService();
                Program.StartService();
            }
        }

        public Program()
        {
            this.ServiceName = Program.SVCNAME;//this shows up in Event Log
            Program._ServiceInstance = this;
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            StartService();
        }

        protected override void OnStop()
        {
            StopService();
            base.OnStop();
        }

        private static void StartService()
        {
            if (_ServiceClassObject == null)
            {
                try
                {
                    _ServiceClassObject = Activator.CreateInstance(_ServiceClass);
                }
                catch (Exception x)
                {
                    WriteToLog("Could not start service: " + x.Message, EventLogEntryType.Error);
                    StopService();
                }
            }
            else
            {
                WriteToLog("Service already instantiated");
            }
        }

        private static void StopService()
        {
            if (_ServiceClassObject != null)
            {
                if (_ServiceClassObject is IDisposable)
                {
                    IDisposable disposable = (IDisposable)_ServiceClassObject;
                    disposable.Dispose();
                }
                _ServiceClassObject = null;
            }
        }

        public static void WriteToLog(string msg)
        {
            WriteToLog(msg, EventLogEntryType.Information);
        }
        public static void WriteToLog(string msg, EventLogEntryType eventType)
        {
            if (_ServiceInstance != null)
            {
                _ServiceInstance.EventLog.WriteEntry(msg, eventType);
            }
            else
            {
                //must be console mode
                Console.WriteLine(eventType.ToString() + ": " + msg);
            }
        }
    }
}
